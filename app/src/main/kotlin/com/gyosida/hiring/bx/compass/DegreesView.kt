package com.gyosida.hiring.bx.compass

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import com.gyosida.hiring.bx.R
import com.gyosida.hiring.bx.sensor.centerHorizontal
import com.gyosida.hiring.bx.sensor.centerVertical

class DegreesView : View {

    private val degreesPainting = DegreesPaintingHolder(context)

    private val northText = context.getString(R.string.direction_north)
    private val eastText = context.getString(R.string.direction_east)
    private val westText = context.getString(R.string.direction_west)
    private val southText = context.getString(R.string.direction_south)

    private val northTextRect = northText.createTextRect(degreesPainting.northDirectionTextPaint)
    private val westTextRect = westText.createTextRect(degreesPainting.directionTextPaint)
    private val eastTextRect = eastText.createTextRect(degreesPainting.directionTextPaint)
    private val southTextRect = southText.createTextRect(degreesPainting.directionTextPaint)

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs, 0)

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        // outline
        canvas.drawCircle(centerHorizontal, centerVertical, centerHorizontal - DIRECTION_THICKNESS / 2, degreesPainting.compassOutlinePaint)

        // north
        canvas.drawLine(centerHorizontal, 0f, centerHorizontal, DIRECTION_THICKNESS, degreesPainting.northDirectionPaint)
        canvas.drawText(northText, centerHorizontal - (northTextRect.width() / 2), DIRECTION_SPACING + (northTextRect.height() / 2) * 2, degreesPainting.northDirectionTextPaint)

        // west
        canvas.drawLine(0f, centerVertical, DIRECTION_THICKNESS, centerVertical, degreesPainting.directionPaint)
        canvas.drawText(westText, DIRECTION_SPACING, centerVertical + westTextRect.height()/2, degreesPainting.directionTextPaint)

        // south
        canvas.drawLine(centerHorizontal, height.toFloat(), centerHorizontal, height.toFloat() - DIRECTION_THICKNESS, degreesPainting.directionPaint)
        canvas.drawText(southText, centerHorizontal - (southTextRect.width() / 2), height.toFloat() - DIRECTION_SPACING - southTextRect.height() / 2, degreesPainting.directionTextPaint)

        // east
        canvas.drawLine(width.toFloat(), centerVertical, width.toFloat() - DIRECTION_THICKNESS, centerVertical, degreesPainting.directionPaint)
        canvas.drawText(eastText, width.toFloat() - DIRECTION_SPACING - eastTextRect.width(), centerVertical + eastTextRect.height()/2, degreesPainting.directionTextPaint)
    }

    private fun String.createTextRect(paint: Paint): Rect {
        val textRect = Rect()
        paint.getTextBounds(this, 0, length, textRect)
        return textRect
    }

    companion object {
        private const val DIRECTION_THICKNESS = 50f
        private const val DIRECTION_SPACING = DIRECTION_THICKNESS + 15f
    }
}