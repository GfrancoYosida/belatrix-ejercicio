package com.gyosida.hiring.bx.sensor

interface SensorHandler {

    fun registerListener(listener: EventListener, eventsRate: Int)

    fun unregisterListener(listener: EventListener)

    interface EventListener {

        fun onSensorChanged(values: FloatArray)
    }
}
