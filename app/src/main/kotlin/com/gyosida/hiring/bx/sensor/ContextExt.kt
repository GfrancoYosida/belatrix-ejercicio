package com.gyosida.hiring.bx.sensor

import android.content.Context
import android.hardware.SensorManager

val Context.sensorManager: SensorManager
    get() = getSystemService(Context.SENSOR_SERVICE) as SensorManager

fun Context.getDimen(dimenId: Int) =
        resources.getDimension(dimenId)

//fun Context.getTextSize(dimenId: Int) =
//        resources.getDimension()