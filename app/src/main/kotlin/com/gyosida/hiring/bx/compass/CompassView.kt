package com.gyosida.hiring.bx.compass

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.hardware.Sensor
import android.hardware.SensorManager
import android.util.AttributeSet
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.widget.FrameLayout
import com.gyosida.hiring.bx.sensor.*

class CompassView : FrameLayout, SensorHandler.EventListener {

    private val sensorHandler: SensorHandler = SimpleSensorHandler(context.sensorManager, Sensor.TYPE_ORIENTATION)
    private val degreesView: DegreesView = DegreesView(context)
    private var currentDegree: Float = 0f
    private val centerMarkPaint = Paint().apply {
        color = Color.WHITE
        style = Paint.Style.STROKE
        strokeWidth = 5f
    }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs, 0)

    init {
        degreesView.layoutParams = FrameLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT
        )
        addView(degreesView)
        setBackgroundColor(Color.TRANSPARENT)
    }

    /**
     * Adapted from https://www.javacodegeeks.com/2013/09/android-compass-code-example.html
     */
    override fun onSensorChanged(values: FloatArray) {
        val degree = Math.round(values[0]).toFloat()
        val rotateAnimation = RotateAnimation(
                currentDegree,
                -degree,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f)

        rotateAnimation.duration = 210
        rotateAnimation.fillAfter = true
        degreesView.startAnimation(rotateAnimation)
        currentDegree = -degree
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val halfSize = 50f
        canvas.drawLines(
                floatArrayOf(
                        centerHorizontal - halfSize, centerVertical, centerHorizontal + halfSize, centerVertical,
                        centerHorizontal, centerVertical - halfSize, centerHorizontal, centerVertical + halfSize
                ),
                centerMarkPaint
        )
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        sensorHandler.unregisterListener(this)
    }

    fun start() {
        sensorHandler.registerListener(this, SensorManager.SENSOR_DELAY_GAME)
    }

    fun stop() {
        sensorHandler.unregisterListener(this)
    }
}