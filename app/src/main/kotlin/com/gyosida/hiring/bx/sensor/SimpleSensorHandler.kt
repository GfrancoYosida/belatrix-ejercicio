package com.gyosida.hiring.bx.sensor

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager

class SimpleSensorHandler(
        private val sensorManager: SensorManager,
        private val sensorType: Int
) : SensorHandler {

    private val listenersMap = mutableMapOf<SensorHandler.EventListener, SensorEventListener>()

    override fun registerListener(listener: SensorHandler.EventListener, eventsRate: Int) {
        val sensorListener = wrapListenerWithSensorEventListener(listener)

        listenersMap[listener] = sensorListener
        sensorManager.registerListener(
                sensorListener,
                sensorManager.getDefaultSensor(sensorType),
                eventsRate
        )
    }

    private fun wrapListenerWithSensorEventListener(listener: SensorHandler.EventListener) =
            object : SensorEventListener {
                override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

                override fun onSensorChanged(event: SensorEvent) {
                    listener.onSensorChanged(event.values)
                }
            }

    override fun unregisterListener(listener: SensorHandler.EventListener) {
        if (listenersMap.containsKey(listener)) {
            sensorManager.unregisterListener(listenersMap[listener])
        }
    }
}