package com.gyosida.hiring.bx

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.gyosida.hiring.bx.sensor.SimpleSensorHandler
import com.gyosida.hiring.bx.sensor.SensorHandler
import com.gyosida.hiring.bx.sensor.sensorManager
import com.gyosida.hiring.bx.sensor.shake.ShakeGestureDetector
import kotlinx.android.synthetic.main.activity_shake_gesture.*
import java.lang.ref.WeakReference

class ShakeGestureActivity : AppCompatActivity(), ShakeGestureDetector.ShakeListener {

    private lateinit var shakeGestureDetector: ShakeGestureDetector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shake_gesture)

        shakeGestureDetector = ShakeGestureDetector(createSensorHandler(), WeakReference(this))
    }

    private fun createSensorHandler(): SensorHandler =
            SimpleSensorHandler(sensorManager, Sensor.TYPE_ACCELEROMETER)

    override fun onShake(shaking: Boolean) {
        shakingFL.setBackgroundColor(if (shaking) Color.RED else Color.TRANSPARENT)
        shakeTitleTV.visibility = if (shaking) View.GONE else View.VISIBLE
    }

    override fun onResume() {
        super.onResume()
        shakeGestureDetector.start()
    }

    override fun onPause() {
        super.onPause()
        shakeGestureDetector.stop()
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, ShakeGestureActivity::class.java)
            context.startActivity(intent)
        }
    }
}
