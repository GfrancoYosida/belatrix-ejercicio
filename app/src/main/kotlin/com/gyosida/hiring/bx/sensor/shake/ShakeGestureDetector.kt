package com.gyosida.hiring.bx.sensor.shake

import android.hardware.SensorManager
import com.gyosida.hiring.bx.sensor.SensorHandler
import java.lang.ref.WeakReference

class ShakeGestureDetector(
        private val sensorHandler: SensorHandler,
        private val shakeListener: WeakReference<ShakeListener>
) : SensorHandler.EventListener {

    private var shakeTimestamp: Long = 0
    private var shakeCount: Int = 0

    /**
     * Shaking gesture calculation grabbed from http://jasonmcreynolds.com/?p=388
     */
    override fun onSensorChanged(values: FloatArray) {
        val listener = shakeListener.get() ?: return
        val x = values[0]
        val y = values[1]
        val z = values[2]
        val gravityX = x / SensorManager.GRAVITY_EARTH
        val gravityY = y / SensorManager.GRAVITY_EARTH
        val gravityZ = z / SensorManager.GRAVITY_EARTH
        val gravitySum = gravityX * gravityX + gravityY * gravityY + gravityZ * gravityZ

        // gForce will be close to 1 when there is no movement.
        val gForce = Math.sqrt(gravitySum.toDouble()).toFloat()

        if (gForce > SHAKE_THRESHOLD_GRAVITY) {
            val now = System.currentTimeMillis()
            // ignore shake events too close to each other (500ms)
            if (shakeTimestamp + SHAKE_SLOP_TIME_MS > now) {
                return
            }

            // reset the shake count after 3 seconds of no shakes
            if (shakeTimestamp + SHAKE_COUNT_RESET_TIME_MS < now) {
                shakeCount = 0
            }

            shakeTimestamp = now
            shakeCount++

            listener.onShake(true)
        } else {
            listener.onShake(false)
        }
    }

    fun start() {
        sensorHandler.registerListener(this, SensorManager.SENSOR_DELAY_NORMAL)
    }

    fun stop() {
        sensorHandler.unregisterListener(this)
    }

    interface ShakeListener {

        fun onShake(shaking: Boolean)
    }

    companion object {
        private const val SHAKE_THRESHOLD_GRAVITY = 1.5f
        private const val SHAKE_SLOP_TIME_MS = 500
        private const val SHAKE_COUNT_RESET_TIME_MS = 3000
    }
}