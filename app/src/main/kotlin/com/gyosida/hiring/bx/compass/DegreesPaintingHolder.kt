package com.gyosida.hiring.bx.compass

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import com.gyosida.hiring.bx.R
import com.gyosida.hiring.bx.sensor.getDimen

internal class DegreesPaintingHolder(context: Context) {

    val directionPaint = Paint().apply {
        color = Color.WHITE
        strokeWidth = context.getDimen(R.dimen.direction_mark_width)
        style = Paint.Style.STROKE
    }
    val directionTextPaint = Paint().apply {
        color = Color.WHITE
        style = Paint.Style.FILL
        textSize = context.getDimen(R.dimen.direction_text_size)
    }
    val northDirectionPaint = Paint(directionPaint).apply {
        color = Color.RED
    }
    val northDirectionTextPaint = Paint(directionTextPaint).apply {
        color = Color.RED
        textSize = context.getDimen(R.dimen.direction_north_text_size)
    }
    val compassOutlinePaint = Paint().apply {
        color = Color.CYAN
        style = Paint.Style.STROKE
        strokeCap = Paint.Cap.SQUARE
        strokeWidth = 2f
        isAntiAlias = true
        isDither = true
    }
}