package com.gyosida.hiring.bx.sensor

import android.view.View

val View.centerVertical
    get() = measuredHeight / 2f

val View.centerHorizontal
    get() = measuredWidth / 2f