package com.gyosida.hiring.bx

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        shakeBT.setOnClickListener { ShakeGestureActivity.start(this) }
        compassBT.setOnClickListener { CompassActivity.start(this) }
    }
}
