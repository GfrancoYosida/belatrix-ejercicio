package com.gyosida.hiring.bx.sensor.shake

import com.gyosida.hiring.bx.sensor.SensorHandler
import com.nhaarman.mockitokotlin2.*
import org.junit.Test

import java.lang.ref.WeakReference

class ShakeGestureDetectorTest {

    private val sensorHandler = mock<SensorHandler>()
    private val shakeListener = mock<ShakeGestureDetector.ShakeListener>()
    private val shakeGestureDetector = ShakeGestureDetector(
            sensorHandler,
            WeakReference(shakeListener)
    )

    @Test
    fun `test on sensor changed when below shaking threshold should invoke listener with shaking as false`() {
        val values = floatArrayOf(0f, 0f, 1f)

        shakeGestureDetector.onSensorChanged(values)

        verify(shakeListener).onShake(false)
    }

    @Test
    fun `test on sensor changed when above shaking threshold should invoke listener with shaking as true`() {
        val values = floatArrayOf(12.7429f, 9.77631f, 0.812349f)

        shakeGestureDetector.onSensorChanged(values)

        verify(shakeListener).onShake(true)
    }

    @Test
    fun `test on start should register itself to sensor handler`() {
        shakeGestureDetector.start()

        verify(sensorHandler).registerListener(eq(shakeGestureDetector), any())
    }

    @Test
    fun `test on stop should unregister itself from sensor handler`() {
        shakeGestureDetector.stop()

        verify(sensorHandler).unregisterListener(shakeGestureDetector)
    }
}