# Belatrix Challenge #
Android application to showcase the questions 2 and 4 from the Belatrix challenge. Please do not take this project as a example of architecturing commercial applications, since the purpose is just showcasing the challenges.
## **Getting Started** ##
### Clone the repository ###
Clon the project at your local machine using the following command line at the terminal:
```
#!unix
git clone https://GfrancoYosida@bitbucket.org/GfrancoYosida/belatrix-ejercicio.git
```
you should be able to clone it since it is a public repository.
### Prerequisites ###
Have installed the following:

* Jdk 7
* Android Studio 3.0+
* Android SDK 28
* Gradle build tools 3.1.4
* Physical Android device (for simplicity when testing the features, which depends on device sensors)

### Open and Run in Android Studio ###
1. Open project in Android Studio.
2. Gradle Build System will configure and install dependencies as described at app/build.gradle file.
3. Connect your physical device or run an emulator instance.
3. Press the Run button.

### **Author** ###
* Gianfranco Yosida

You can reach me out at gianfranco.yosida@gmail.com
